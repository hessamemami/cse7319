var kafka = require('kafka-node');
var Consumer = kafka.Consumer;
var options = require('./KafkaConsumerConfigs');
var client = new kafka.KafkaClient(options.server);
var amsConsumer = function (callback) {

	let consumer = new Consumer(client, [{
		topic: options.topic,
		partition: 0
	}], {
		autoCommit: options.autoCommit
	});

	consumer.on('message', function (message) {
    	callback(message);
	});
};

module.exports = amsConsumer;
