var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var amsConsumer = require('./ClientConsumer');
var amsProducer = require('./ClientProducer');
var business = require("./business");
var path = require('path');

app.use(express.static(__dirname + '/node_modules'));
app.use(express.static(__dirname + '/client/assets'));



app.get('/', function (req, res, next) {
	res.sendFile(__dirname + '/client/index.html');
});


io.on('connection', function (client) {
	console.log('Client connected...');

	client.on('action', function (data) {
        
        //client.emit('show', 'this is a data');
        amsProducer('{"type":"engine-client-IN" , "payload": "ad1"}');

	});


});

amsConsumer(business);
server.listen(4200);


