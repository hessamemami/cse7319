var client = {

  init : function(){
    this.socket = io.connect();
    this.buttons = $(".travel-button");
    this.buttons.on('click', $.proxy(this.sendMessage, this));
    this.initSocket();
  },
  initSocket : function(){
    
  
		this.socket.on('show', $.proxy(function (data) {
      console.log('show the message' + data);
    },this));
    

  }, 
  sendMessage : function(e){
    e.preventDefault();
    this.socket.emit('action', 'travel');
    
  }
};
$(document).ready(function () {

  client.init();
  
});
