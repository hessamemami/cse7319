var kafka = require('kafka-node');
var Producer = kafka.Producer;
var options = require('./KafkaConsumerConfigs');



var amsProducer = function (message) {
    
    var client = new kafka.KafkaClient(options.server);
    
    let prodcuer = new Producer(client);
    
	prodcuer.on('ready', function () {
    
		let payloads = [{
			topic: options.topic,
			messages: message
        }];
		prodcuer.send(payloads, (error, data) => {
			console.log(data);
		});
	});
};

module.exports = amsProducer;
