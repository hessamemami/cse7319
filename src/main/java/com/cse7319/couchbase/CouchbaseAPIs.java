package com.cse7319.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CouchbaseAPIs {

    private final Cluster cluster;
    private final Bucket bucket;
    private final List<String> nodes;
    private String documentIdPrefix;

    public CouchbaseAPIs(String servers, String username, String password, String bucketName, String documentIdPrefix) {

        nodes = Arrays.asList(servers);
        CouchbaseEnvironment environment = DefaultCouchbaseEnvironment.create();
        cluster = CouchbaseCluster.create(environment, nodes);
        cluster.authenticate(username, password);
        bucket = cluster.openBucket(bucketName);
        bucket.bucketManager().createN1qlPrimaryIndex(true, false);
        this.documentIdPrefix = documentIdPrefix;

    }

    public void upsert(JsonObject data) {
        UUID guid =  UUID.randomUUID();
        bucket.upsert(JsonDocument.create(documentIdPrefix + guid.toString(), data));
    }

    public void delete(String property, String value) {

        N1qlQueryResult result = bucket.query(
                N1qlQuery.simple(
                        "DELETE FROM "+ bucket.name() +"\n" +
                                "WHERE "+ property +" = \"" + value +"\"\n" +
                                "RETURNING meta().id"
                ));

    }

    public N1qlQueryResult getAll() {

        N1qlQueryResult result = bucket.query(
                N1qlQuery.simple("SELECT * FROM " + bucket.name() + " WHERE meta(" + bucket.name() + ").id"));

        return result;

    }

    public N1qlQueryResult getByDocId(String documentId) {

        N1qlQueryResult result = bucket.query(
                N1qlQuery.simple("SELECT * FROM " + bucket.name() + " WHERE meta(" + bucket.name() + ").id = \"" + documentIdPrefix + documentId + "\" "));

        return result;

    }

    public void disconnect() {
        cluster.disconnect();
    }

}
