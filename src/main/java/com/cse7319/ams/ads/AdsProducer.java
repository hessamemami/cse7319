package com.cse7319.ams.ads;

import com.cse7319.kafka.ProducerAPI;

public class AdsProducer {

    ProducerAPI producer;

    public AdsProducer() {
        producer = new ProducerAPI(AdsConfigs.TOPIC);
    }

    public void send(String value) {
        producer.send(value);
    }
}
