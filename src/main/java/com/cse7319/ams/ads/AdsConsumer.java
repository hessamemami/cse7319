package com.cse7319.ams.ads;

import com.cse7319.kafka.ConsumerRunnable;
import com.cse7319.kafka.KafkaActionsInf;

public class AdsConsumer {


    private ConsumerRunnable consumerRunnable;

    public AdsConsumer() {


        final KafkaActionsInf kafkaActions = new AdsKafkaActionsImp();

        consumerRunnable = new ConsumerRunnable(AdsConfigs.TOPIC, AdsConfigs.GROUPID, kafkaActions);


    }

    public void start() {

        Thread thread = new Thread(consumerRunnable);
        thread.start();

    }

}
