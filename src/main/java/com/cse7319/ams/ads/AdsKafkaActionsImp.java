package com.cse7319.ams.ads;

import com.cse7319.couchbase.CouchbaseAPIs;
import com.cse7319.couchbase.CouchbaseConfigs;
import com.cse7319.kafka.KafkaActionsInf;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class AdsKafkaActionsImp implements KafkaActionsInf {


    @Override
    public void process(ConsumerRecord<String, String> record) {

        if (!record.value().contains(AdsConfigs.FILTER_IN)) {
            return;
        }


        AdsProducer adsProducer = new AdsProducer();
        CouchbaseAPIs couchbaseAPIs = new CouchbaseAPIs(CouchbaseConfigs.server, CouchbaseConfigs.username, CouchbaseConfigs.password, AdsConfigs.BUKET, AdsConfigs.DOCUMENTID);
        String data = couchbaseAPIs.getAll().allRows().toString();

        data = "[{ \"type\": \"" + AdsConfigs.FILTER_OUT + "\",\"payload\":"  + data + "}]";

        adsProducer.send(data);
        couchbaseAPIs.disconnect();


    }
}
