package com.cse7319.ams.ads;

public interface AdsConfigs {

    String TOPIC = "com-cse7319-ams";
    String GROUPID = "ads-persistence";
    String FILTER_OUT = "ads-persistence_OUT";
    String FILTER_IN = "ads-persistence_IN";
    String BUKET = "Ads";
    String DOCUMENTID = "ads::";
}
