package com.cse7319.ams.rules.engine;

import com.cse7319.kafka.ConsumerRunnable;
import com.cse7319.kafka.KafkaActionsInf;

public class EngineConsumer {

    private ConsumerRunnable consumerRunnable;

    public EngineConsumer() {


        final KafkaActionsInf kafkaActions = new EngineImp();

        consumerRunnable = new ConsumerRunnable(EngineConfigs.TOPIC, EngineConfigs.GROUPID, kafkaActions);


    }

    public void start() {

        Thread thread = new Thread(consumerRunnable);
        thread.start();

        EngineProducer engineRulesProducer = new EngineProducer();
        EngineProducer engineAdsProducer = new EngineProducer();

        engineRulesProducer.send(EngineConfigs.FILTER_RULE);
        engineAdsProducer.send(EngineConfigs.FILTER_ADS);

    }
}
