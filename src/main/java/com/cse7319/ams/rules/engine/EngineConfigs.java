package com.cse7319.ams.rules.engine;

public interface EngineConfigs {
    String TOPIC = "com-cse7319-ams";
    String GROUPID = "engine-persistence";
    String[] FILTER_IN = {"rules-persistence_OUT" ,"ads-persistence_OUT"};
    String CLIENT_IN = "engine-client-IN";
    String CLIENT_OUT = "engine-client-OUT";
    String FILTER_RULE = "rules-persistence_IN";
    String FILTER_ADS = "ads-persistence_IN";
}
