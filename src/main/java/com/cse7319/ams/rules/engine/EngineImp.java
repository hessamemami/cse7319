package com.cse7319.ams.rules.engine;


import com.cse7319.ams.ads.Ad;
import com.cse7319.ams.rules.persistence.Rule;
import com.cse7319.kafka.KafkaActionsInf;

import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EngineImp implements KafkaActionsInf {

    List<Rule> rules = new ArrayList<>();
    List<Ad> ads = new ArrayList<>();

    @Override
    public void process(ConsumerRecord<String, String> record) {

        if (!stringContainsItemFromList(record.value(), EngineConfigs.FILTER_IN)) {
            try {
                action(record);
            } catch (Exception e) { } finally { }

        } else {


            JsonArray jsonArray = new Gson().fromJson(record.value(), JsonArray.class);
            JsonObject jsonObject = (JsonObject) jsonArray.get(0);
            JsonArray jsonPayload = jsonObject.getAsJsonArray("payload");
            JsonToRule(jsonPayload);
            JsonToAd(jsonPayload);
        }


    }


    private boolean stringContainsItemFromList(String inputStr, String[] items) {
        return Arrays.stream(items).parallel().anyMatch(inputStr::contains);
    }


    private void JsonToRule(JsonArray jsonArray) {

        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                String jRule = jsonArray.getAsJsonArray().get(i).getAsJsonObject().get("Rules").toString();
                this.rules.add(new Gson().fromJson(jRule, Rule.class));
            } catch (Exception e) {

            }
        }

    }

    private void JsonToAd(JsonArray jsonArray) {

        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                String jRule = jsonArray.getAsJsonArray().get(i).getAsJsonObject().get("Ads").toString();
                this.ads.add(new Gson().fromJson(jRule, Ad.class));
            } catch (Exception e) {

            }
        }

    }

    private Client processClientMessages(ConsumerRecord<String, String> record) {
        Gson g = new Gson();
        Client client;
        try {
            client = g.fromJson(record.value(), Client.class);
        } catch (Exception e) {
            client = null;
        }

        return client;


    }


    private List<Ad> engine(Client client) {

        Pattern rulePattern = Pattern.compile("'([^']*)'");
        List<Ad> ads = new ArrayList<>();


        for (Rule rule : this.rules) {

            String ruleExpresion = rule.getRule();
            Matcher m = rulePattern.matcher(ruleExpresion);
            int i = 0;
            while (m.find()) {
                if (i == 1) {
                    String adId = m.group(0).replace("'", "");
                    for (Ad ad : this.ads) {

                        if (ad.getId().equals(adId)) {
                            Ad data = new Ad();
                            data.setType(ad.getType());
                            data.setId(ad.getId());
                            data.setAd(ad.getAd());
                            ads.add(data);
                        }
                    }
                }
                i++;
            }

        }

        return ads;
    }

    private void action(ConsumerRecord<String, String> record) {

        if (!record.value().contains(EngineConfigs.CLIENT_IN)) {
            System.out.println("record" + record.value());
            return;
        }
        Client clinet = processClientMessages(record);
        List<Ad> clienAds = engine(clinet);
        EngineProducer engineClientProducer;
        for (Ad ad : clienAds) {
            engineClientProducer = new EngineProducer();
            ObjectMapper mapper = new ObjectMapper();
            try {
                engineClientProducer.send("[{ \"type\": \"" + EngineConfigs.CLIENT_OUT + "\",\"payload\":" + mapper.writeValueAsString(ad) + "}]");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }
}
