package com.cse7319.ams.rules.engine;

import com.cse7319.kafka.ProducerAPI;

public class EngineProducer {

    ProducerAPI producer;

    public EngineProducer() {
        producer = new ProducerAPI(EngineConfigs.TOPIC);
    }

    public void send(String value) {
        producer.send(value);
    }
}
