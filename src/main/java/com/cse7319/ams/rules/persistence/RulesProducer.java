package com.cse7319.ams.rules.persistence;

import com.cse7319.kafka.ProducerAPI;

public class RulesProducer {

    ProducerAPI producer;

    public RulesProducer() {
        producer = new ProducerAPI(RulesConfigs.TOPIC);
    }

    public void send(String value) {
        producer.send(value);
    }
}
