package com.cse7319.ams.rules.persistence;

import com.cse7319.kafka.ConsumerRunnable;
import com.cse7319.kafka.KafkaActionsInf;

public class RulesConsumer {


    private ConsumerRunnable consumerRunnable;

    public RulesConsumer() {


        final KafkaActionsInf kafkaActions = new RulesKafkaActionsImp();

        consumerRunnable = new ConsumerRunnable(RulesConfigs.TOPIC, RulesConfigs.GROUPID, kafkaActions);


    }

    public void start() {

        Thread thread = new Thread(consumerRunnable);
        thread.start();

    }

}
