package com.cse7319.ams.rules.persistence;

import com.cse7319.couchbase.CouchbaseAPIs;
import com.cse7319.couchbase.CouchbaseConfigs;
import com.cse7319.kafka.KafkaActionsInf;
import org.apache.kafka.clients.consumer.ConsumerRecord;



public class RulesKafkaActionsImp implements KafkaActionsInf {


    @Override
    public void process(ConsumerRecord<String, String> record) {

        if (!record.value().contains(RulesConfigs.FILTER_IN)) {
            return;
        }

        RulesProducer rulesProducer = new RulesProducer();
        CouchbaseAPIs couchbaseAPIs = new CouchbaseAPIs(CouchbaseConfigs.server, CouchbaseConfigs.username, CouchbaseConfigs.password, RulesConfigs.BUKET, RulesConfigs.DOCUMENTID);
        String data = couchbaseAPIs.getAll().allRows().toString();


        data = "[{ \"type\": \"" + RulesConfigs.FILTER_OUT + "\",\"payload\":"  + data + "}]";
        rulesProducer.send(data);
        couchbaseAPIs.disconnect();


    }
}
