package com.cse7319.ams.rules.persistence;

public interface RulesConfigs {

    String TOPIC = "com-cse7319-ams";
    String GROUPID = "rules-persistence";
    String FILTER_OUT = "rules-persistence_OUT";
    String FILTER_IN = "rules-persistence_IN";
    String BUKET = "Rules";
    String DOCUMENTID = "rules::";
}
