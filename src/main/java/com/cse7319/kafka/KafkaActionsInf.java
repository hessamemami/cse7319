package com.cse7319.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface KafkaActionsInf {

    void process (ConsumerRecord<String, String> record);
    String READ = "read";
    String WERITE = "write";

}
