package com.cse7319.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerRunnable implements Runnable {

    private String topic;
    private String groupId;
    private Properties properties = new Properties();
    private String bootstrapServers = "0.0.0.0:9092";
    private KafkaConsumer<String, String> consumer;
    private KafkaActionsInf kafkaActions;


    public ConsumerRunnable(String topic, String groupId,  KafkaActionsInf kafkaActions) {

        this.topic = topic;
        this.groupId = groupId;
        this.kafkaActions = kafkaActions;

        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, this.groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        this.consumer = new KafkaConsumer<>(properties);
        this.consumer.subscribe(Arrays.asList(this.topic));


    }

    @Override
    public void run() {

        while (true) {

            ConsumerRecords<String, String> records = consumer.poll(100);

            for (ConsumerRecord<String, String> record : records) {
                if (record.value() != null) {
                    kafkaActions.process(record);
                }
            }

            consumer.commitSync();

        }
    }
}
