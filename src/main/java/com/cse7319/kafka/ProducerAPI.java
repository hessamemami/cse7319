package com.cse7319.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


import java.util.Properties;

public class ProducerAPI {

    private String topic;

    private Properties properties = new Properties();
    private String bootstrapServers = "0.0.0.0:9092";
    private KafkaProducer<String, String> producer;


    public ProducerAPI(String topic) {

        this.topic = topic;

        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        producer = new KafkaProducer<>(properties);


    }


    public void send(String value) {

        ProducerRecord<String, String> record = new ProducerRecord<>(topic, value);

        producer.send(record);
        producer.flush();
        producer.close();

    }
}
