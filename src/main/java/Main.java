import com.cse7319.ams.ads.AdsConsumer;
import com.cse7319.ams.rules.engine.EngineConsumer;
import com.cse7319.ams.rules.persistence.RulesConsumer;


public class Main {

    public static void main(String[] args) {

        RulesConsumer rulesConsumer = new RulesConsumer();
        rulesConsumer.start();

        AdsConsumer adsConsumer = new AdsConsumer();
        adsConsumer.start();

        EngineConsumer engineConsumer = new EngineConsumer();
        engineConsumer.start();



    }
}
